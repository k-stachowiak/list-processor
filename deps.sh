#!/usr/bin/env bash
function deps_for_file {
    echo $(cpp -M $1 | sed -e 's| \\|\n|g' -e 's| |\n|g' | sed '/^$/d' | grep -v usr | tr '\n' ' ')
}
export -f deps_for_file
find . -name '*.cpp' -exec bash -c 'deps_for_file "$0"' {} \;
