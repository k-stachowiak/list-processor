#!/usr/bin/env bash

make clean && make -j4 && valgrind --leak-check=full ./list-processor test
RESULT=$?
echo "The result was: $RESULT"
