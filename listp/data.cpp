#include "data.hpp"
#include "algorithm.hpp"

// Note: This is a circular dependency only needed for the debug printing:
#include "parse.hpp"

#include <iostream>
#include <algorithm>

namespace {

bool test_list_pool_algorithms() {

    ListPool pool;

    // 1. Prepare test data:

    // 1.1. Expected elements lists:
    //      (we'll be crossing things out from these)
    std::vector<int> expected_1 { 1, 2, 3 };
    std::vector<std::pair<int, int>> expected_2 {
        { 1, 1 }, { 2, 2 }, { 3, 3 }
    };

    // 1.2. Prepare the pool managed structures:
    //      (note, how we remember location of the element containing the value
    //       two)
    PointerType list_1 = NIL;
    list_1 = pool.cons(Value::make_integer(3), list_1);
    list_1 = pool.cons(Value::make_integer(2), list_1);
    list_1 = pool.cons(Value::make_integer(1), list_1);
    PointerType list_2 = NIL;
    list_2 = pool.cons(Value::make_integer(3), list_2);
    list_2 = pool.cons(Value::make_integer(2), list_2);
    list_2 = pool.cons(Value::make_integer(1), list_2);

    // 2. Test the double for_each algorithm:
    ListPool::ConstIterator
        it_first { pool, list_1 }, it_last {},
        jt_first { pool, list_2 }, jt_last {};
    for_each_2(
        it_first, it_last, jt_first, jt_last,
        [&expected_2](const Value &x, const Value &y) {
            expected_2.erase(
                std::remove(expected_2.begin(),
                            expected_2.end(),
                            std::make_pair(x.integer, y.integer)),
                expected_2.end());
        });
    if (!expected_2.empty()) {
        std::cerr << "Failed clearing expected values with for_each_2" << std::endl;
        return false;
    }

    // 3. If we have got here, report success:
    return true;
}

bool test_list_equals(const ListPool &pool,
                      PointerType actual,
                      const std::vector<Value> &expected) {

    auto it = actual;
    auto jt = expected.begin();
    bool success = true;
    while (it != NIL && jt != expected.end()) {
        if (pool.car(it) != *jt) {
            success = false;
            break;
        }
        it = pool.cdr(it);
        ++jt;
    }
    success = success && (it == NIL && jt == expected.end());
    if (!success) {
        std::cerr << "List comparison failed. Expected: " << std::endl;
        std::cerr << "\texpected: " << to_string(expected, pool) << std::endl;
        std::cerr << "\tactual: " << to_string(Value::make_list(actual), pool) << std::endl;
    }
    return success;
}

}

char *string_copy(const char *x) {
    int len = std::strlen(x);
    char *result = new char[len + 1];
    std::copy(x, x + len + 1, result);
    return result;
}

bool equals(const Value &x, const Value &y, const ListPool &pool)
{
    PointerType it, jt;
    if (x.type != y.type) return false;
    switch (x.type) {
    case Value::Type::Symbol:
    case Value::Type::Boolean:
    case Value::Type::Integer:
    case Value::Type::Floating:
    case Value::Type::Sentinel:
        return x == y;
    case Value::Type::List:
        it = x.list;
        jt = y.list;
        while (it != NIL && jt != NIL) {
            if (!equals(pool.car(it), pool.car(jt), pool)) return false;
            it = pool.cdr(it);
            jt = pool.cdr(jt);
        }
        return it == NIL && jt == NIL;
    }
    throw std::runtime_error { "Malformed enum encountered" };
}

bool test_value() {
    // 1. Test polite values creation and assignment:
    Value x = Value::make_symbol("TIC");
    Value y = Value::make_symbol("TAC");
    Value z = Value::make_symbol("TOE");
    x = y;
    if (std::strcmp(x.symbol, "TAC") != 0) {
        std::cerr << "Copy assignment of a symbol value failed" << std::endl;
        return false;
    }
    x = std::move(z);
    if (std::strcmp(x.symbol, "TOE") != 0) {
        std::cerr << "Move assignment of a symbol value failed" << std::endl;
        return false;
    }

    // 2. Test chain assignment:
    Value s, t, u;
    s = t = u = Value::make_symbol("FOO");
    if (std::strcmp(s.symbol, t.symbol) != 0 ||
        std::strcmp(t.symbol, u.symbol) != 0 ||
        std::strcmp(u.symbol, "FOO") != 0) {
        std::cerr << "Chain assignment of value failed" << std::endl;
        return false;
    }

    // 3. Test storing in standard containers:
    std::vector<Value> v;
    v.push_back(Value::make_symbol("ONE"));
    v.push_back(Value::make_integer(2));
    v.push_back(Value::make_floating(3.0));
    std::vector<Value> w = v;
    if (!std::equal(v.begin(), v.end(), w.begin())) {
        std::cerr << "Copying a value vector failed" << std::endl;
        return false;
    }

    return true;
}

bool test_list_pool_basics() {

    /*
     * Note: This is translation unit local, but not inside an anonymous
     *       namespace as it is a friend to a public class.
     */

    ListPool pool;

    // 1. Prepare a simple list:
    PointerType list = NIL;
    list = pool.cons(Value::make_integer(3), list);
    list = pool.cons(Value::make_integer(2), list);
    list = pool.cons(Value::make_integer(1), list);

    // 2. Evaluate the list contents correctness:
    PointerType temp = list;
    if (pool.car(temp).type != Value::Type::Integer || pool.car(temp).integer != 1)
        goto failure;
    temp = pool.cdr(temp);
    if (pool.car(temp).type != Value::Type::Integer || pool.car(temp).integer != 2)
        goto failure;
    temp = pool.cdr(temp);
    if (pool.car(temp).type != Value::Type::Integer || pool.car(temp).integer != 3)
        goto failure;
    temp = pool.cdr(temp);
    if (temp != NIL)
        goto failure;

    // 3. Check the releasing operation:

    // 3.1. Release the test list:
    while (list != NIL)
        list = pool.release(list);
    // 3.2. Check the garbage state:
    if (pool.length(pool.garbage) != 3)
        goto failure;
    // 3.3. Rebuild a list from garbage:
    list = pool.cons(Value::make_floating(3), list);
    list = pool.cons(Value::make_floating(2), list);
    list = pool.cons(Value::make_floating(1), list);
    // 3.4. Check if garbage has been reused:
    if (pool.length(pool.garbage) != 0)
        goto failure;

    // 4. Report success:
    return true;

failure:
    std::cerr << "Failed list pool test" << std::endl;
    return false;
}

bool test_list_pool_reverse() {

    /*
     * Note: This is translation unit local, but not inside an anonymous
     *       namespace as it is a friend to a public class.
     */

    ListPool pool;
    std::vector<Value> expected {
        Value::make_integer(3),
        Value::make_integer(2),
        Value::make_integer(1)
    };

    // 1. Prepare a simple list:
    PointerType list = NIL;
    list = pool.cons(Value::make_integer(3), list);
    list = pool.cons(Value::make_integer(2), list);
    list = pool.cons(Value::make_integer(1), list);
    // Note: at this point, list should be (1 2 3)

    // 2. Create and evaluate a reversed copy:
    PointerType copy = pool.reverse_copy(list);
    if (!test_list_equals(pool, copy, expected)) {
        std::cerr << "Failed list pool reversed copy test" << std::endl;
        return false;
    }

    // 3. Create and evaluate an in-place reversed list:
    list = pool.reverse(list);
    if (!test_list_equals(pool, copy, expected)) {
        std::cerr << "Failed list pool in-place reverse test" << std::endl;
        return false;
    }

    // 4. There should have been exactly 6 cons cells created:
    if (pool.impl.size() != 6) {
        std::cerr << "Unexpected side effects in list pool test" << std::endl;
        return false;
    }

    // 5. There should be no garbage at this point:
    if (pool.garbage != NIL) {
        std::cerr << "Unexpected side effects in list pool test" << std::endl;
        return false;
    }

    // 6. If we've got here, report success:
    return true;
}

bool test_list_pool() {
    return
        test_list_pool_basics() &&
        test_list_pool_reverse() &&
        test_list_pool_algorithms();
}
