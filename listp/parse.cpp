#include "parse.hpp"

#include <sstream>

namespace {

bool test_parser_atom(const std::string &source, const Value &expected) {
    ListPool pool;
    Value actual;
    auto first = source.begin(), last = source.end();
    if (!parse_expression(first, last, pool, actual)) {
        std::cerr << "Failed parsing atom parsing test expression: " <<
            source << std::endl;
        return false;
    }
    if (actual != expected) {
        std::cerr << "Atom parsing test failed." <<
            " Expected: " << to_string(expected, pool) <<
            ", actual: " << to_string(actual, pool) << std::endl;
        return false;
    }
    return true;
}

bool test_parser_flat_list(const std::string &source,
                           const std::vector<Value> &expected) {
    ListPool pool;
    Value actual;
    auto first = source.begin(), last = source.end();
    if (!parse_expression(first, last, pool, actual)) {
        std::cerr << "Failed parsing list parsing test expression: " <<
            source << std::endl;
        return false;
    }
    if (actual.type != Value::Type::List) {
        std::cerr << "Failed parsing list parsing test expression: " <<
            source << "; unexpected non-listst result" << std::endl;
        return false;
    }
    auto it = actual.list;
    auto jt = expected.begin();
    while (it != NIL && jt != expected.end()) {
        if (pool.car(it) != *jt) {
            std::cerr << "Failed parsing list test element value." <<
                " Expected element: " << to_string(*jt, pool) <<
                ", actual: " << to_string(pool.car(it), pool) << std::endl;
            return false;
        }
        it = pool.cdr(it);
        ++jt;
    }
    if (it != NIL || jt != expected.end()) {
        std::cerr << "Incorrect actual elements in a list test" << std::endl;
        return false;
    }
    return true;
}

bool test_parser_complex_list() {
    ListPool pool;
    Value expected, actual;
    std::string source = "(1 2.0 (true nil))";
    auto first = source.begin(), last = source.end();
    PointerType sub_list = NIL;
    sub_list = pool.cons(Value::make_list(NIL), sub_list);
    sub_list = pool.cons(Value::make_boolean(true), sub_list);
    PointerType expected_list = NIL;
    expected_list = pool.cons(Value::make_list(sub_list), expected_list);
    expected_list = pool.cons(Value::make_floating(2), expected_list);
    expected_list = pool.cons(Value::make_integer(1), expected_list);
    expected = Value::make_list(expected_list);
    if (!parse_expression(first, last, pool, actual)) {
        std::cerr << "Failed parsing complex list expression: " <<
            source << std::endl;
        return false;
    }
    if (!equals(actual, expected, pool)) {
        std::cerr << "Complex list parsing test failed." <<
            " Expected: " << to_string(expected, pool) <<
            ", actual: " << to_string(actual, pool) << std::endl;
        return false;
    }
    return true;
}

}

Value parse_atom(const std::string &x) {

    /* ## Parse atom string into a Value object
     *
     * The following case groups are considered in the algorithm:
     * - special literals: e.g. "nil",
     * - numeric values: matching <numeric>
     * - symbols: anything else.
     *
     * ### Precondition:
     * - x is not empty.
     *
     * ### Arguments:
     * - x: string to be parsed.
     *
     * ### Returns: a Value object representing the input string.
     *
     * ### Notes:
     * - This parser doesn't check the input value's range, only the format. The
     *   resulting values depend on the standard library's parsers behavior.
     */

    // 0. Check Preconditions:
    assert(x.empty() == false);

    // 1. Check for special literals:
    if (x == "nil")   return Value::make_list(NIL);
    if (x == "true")  return Value::make_boolean(1);
    if (x == "false") return Value::make_boolean(0);

    // 2. Prepare for the format analysis:
    auto first = x.begin(), last = x.end();

    // 3. Optionally skip a sign symbol:
    if (*first == '+' || *first == '-') ++first;

    // 4. Maybe we already have a symbol (e.g. "+"):
    if (first == last) return Value::make_symbol(x);

    // 5. Look for a period:
    auto it = std::find(first, last, '.');
    if (it == last)
        // 5.a) If no period found, then this is...:
        if (std::all_of(first, last, isdigit))
            // 5.a)a) ...an integer if only consists of digits:
            return Value::make_integer(std::stoi(x));
        else
            // 5.a)b) ...a symbol otherwise:
            return Value::make_symbol(x);
    else
        // 5.b) if period found, then this is...:
        if (std::distance(first, it) > 0 &&
            std::all_of(first, it, isdigit) &&
            std::distance(it + 1, last) > 0 &&
            std::all_of(it + 1, last, isdigit))
            // 5.b)a) ...a floating point number if the period is surrounded solely
            //        by digits:
            return Value::make_floating(std::stod(x));
        else
            // 5.b)b) ...a symbol otherwise:
            return Value::make_symbol(x);
}

bool test_parser() {
    return
        test_parser_atom("1", Value::make_integer(1)) &&
        test_parser_atom("2.0", Value::make_floating(2)) &&
        test_parser_atom("nil", Value::make_list(NIL)) &&
        test_parser_flat_list(
            "(1 2.0 nil)",
            {
                Value::make_integer(1),
                Value::make_floating(2),
                Value::make_list(NIL)
            }) &&
        test_parser_complex_list();
}

std::string to_string(const Value &x, const ListPool &pool) {

    /*
     * ## Converts a given value to a string.
     *
     * If the value is a list, recursive calls are made.
     */

    std::stringstream ss;
    switch (x.type) {
    case Value::Type::Symbol:
        ss << x.symbol;
        break;
    case Value::Type::Boolean:
        ss << ((x.boolean) ? "true" : "false");
        break;
    case Value::Type::Integer:
        ss << x.integer << "i";
        break;
    case Value::Type::Floating:
        ss << x.floating << "f";
        break;
    case Value::Type::List:
        if (x.list == NIL) {
            ss << "nil";
        } else {
            PointerType it = x.list;
            ss << "( ";
            while (it != NIL) {
                ss << to_string(pool.car(it), pool) << " ";
                it = pool.cdr(it);
            }
            ss << ")";
        }
        break;
    case Value::Type::Sentinel:
        ss << "sentinel(" << x.sentinel_address << ")";
        break;
    }
    return ss.str();
}

std::string to_string(const std::vector<Value> &values, const ListPool &pool) {

    /*
     * ## Converts a range of values to string.
     */

    std::stringstream ss;
    ss << "( ";
    for (const Value &value : values)
        ss << to_string(value, pool) << " ";
    ss << ")";
    return ss.str();
}
