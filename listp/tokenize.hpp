#ifndef TOKENIZER_HPP
#define TOKENIZER_HPP

#include "config.hpp"

#include <string>
#include <algorithm>

#include <cassert>

template <class It>
std::string next_token(It &first, It last) {

    /*
     * ## Get the next token in the range of characters.
     *
     * ### Template arguments:
     * - It:       a forward iterator over a type compatible with char.
     *
     * ### Arguments:
     * - first:   a reference to the iterator denoting the beginning of the
     *            analyzed range.
     * - last:    an iterator denoting the end of the analyzed range.
     *
     * ### Returns: the token that has been found.
     *
     * ### Preconditions:
     * 1. first != last,
     * 2. there is a token in the range.
     *
     * ### Postconditions:
     * 1.a) EITHER first == last,
     * 1.b) OR     there is still a token in the stream.
     *
     * ### Notes:
     * - In order to satisfy the preconditions (initially), one should remove
     *   the leading or trailing whitespaces from the range, and verify, whether
     *   whatever remains is not empty.
     * - Postconditions guarantee that if first != last after a call to this
     *   function, then it is valid to call it again.
     */

    // 0. Check Preconditions:
    assert(first != last);
    assert(std::find_if_not(first, last, isspace) != last);

    // 1. Read the token (based on Precondition 2. there must be at least one):
    std::string accumulator;
    while (first != last) {
        if (isspace(*first)) {
            // 1.a)1.: We encounter a space; unconditionally move on to the next
            //         character:
            ++first;
            // 1.a)2.: If we have accumulated anything before the current space,
            //         we return it as a result, and we do not increment first:
            if (!accumulator.empty()) {
                // We secure Postconditions before returning:
                first = std::find_if_not(first, last, isspace);
                return accumulator;
            }
        } else if (*first == '(' || *first == ')' || *first == '\'') {
            // 1.b): We encountered a hard token:
            if (accumulator.empty()) {
                // 1.b)a): If we have not accumulated anything, we return this
                //         hard token as a result, and secure Postconditions:
                accumulator.push_back(*first++);
                first = std::find_if_not(first, last, isspace);
                return accumulator;
            } else {
                // 1.b)b): We have accumulated somethind, so we return the
                //         accumulator, and leave the hard token for further
                //         parsing. Since *first is not a white space, we have
                //         secured Postcondition 1.b).
                return accumulator;
            }
        } else {
            // 1.c): There is no reason to break accumulating a soft (non-hard)
            //       token, so we do just that, and let the loop continue.
            accumulator.push_back(*first++);
        }
    }

    // 2. If this function has been called on a range that contains only one
    //    token, and it is not hard, we will reach this point, at which we
    //    return whatever has been accumulated. Getting here means that we
    //    have met Postcondition 1.a):
    return accumulator;
}

bool test_tokenizer();

#endif
