#include "machine.hpp"
#include "algorithm.hpp"

#include <stdexcept>

namespace {

bool evaluate_list(Machine &machine, PointerType in, PointerType &out) {

    /*
     * ## Execute a list of values and return a list of results.
     */

    // 1. Initialize output list:
    out = NIL;

    // 2. Iterate over input:
    bool error = false;
    std::for_each(
        ListPool::ConstIterator { machine.pool, in }, ListPool::ConstIterator {},
        [&machine, &error, &out](const Value &value_in) {

            // 2.1. Break on error:
            if (error) return;

            // 2.2. Evaluate current element:
            Value value_out;
            if (!execute_value(value_in, machine, value_out))
                error = true;

            // 2.3. Append the element to the resulting list:
            out = machine.pool.cons(value_out, out);
        });

    // 3. Reverse the result to restore the original order:
    out = machine.pool.reverse(out);

    // 3. Report success or failure:
    return !error;
}

template <class Op>
bool common_numeric_unary(
        PointerType arguments,
        Machine &machine,
        Op op,
        Value &result) {

    /*
     * ## Boilerplate necessary for execution of an unary numeric operator.
     */

    // 1. Assert input:
    if (arguments == NIL || machine.pool.cdr(arguments) != NIL) {
        std::cerr << "Invalid number of arguments passed to a unary function" << std::endl;
        return false;
    }

    // 2. Perform the operation according to the input type, or report error if
    //    input of invalid type passed:
    Value argument = machine.pool.car(arguments);
    switch (argument.type) {
    case Value::Type::Integer:
        result = Value::make_integer(op(argument.integer));
        return true;
    case Value::Type::Floating:
        result = Value::make_floating(op(argument.floating));
        return true;
    default:
        std::cerr << "Non-numeric argument passed to a numeric function" << std::endl;
        return false;
    }

    // 3. Handle obscure case of "ORing" enums; this should never be done with
    //    Value::type member:
    throw std::runtime_error { "Malformed enum encountered" };
}

template <class Op>
bool common_numeric_binary(
        PointerType arguments,
        Machine &machine,
        Op op,
        Value &result) {

    /*
     * ## Boilerplate necessary for execution of a binary numeric operator.
     */

    // 1. Read the first argument:
    if (arguments == NIL) {
        std::cerr << "Not enough arguments passed to a binary operator" << std::endl;
        return false;
    }
    const Value &x = machine.pool.car(arguments);
    arguments = machine.pool.cdr(arguments);

    // 2. Read the second argument:
    if (arguments == NIL) {
        std::cerr << "Not enough arguments passed to a binary operator" << std::endl;
        return false;
    }
    const Value &y = machine.pool.car(arguments);
    arguments = machine.pool.cdr(arguments);

    // 3. Make sure there are no more arguments:
    if (arguments != NIL) {
        std::cerr << "Too many arguments passed to a binary operator" << std::endl;
        return false;
    }

    // 4. Make sure the types match:
    if (x.type != y.type) {
        std::cerr << "Arguments of different types passed to a binary operator" << std::endl;
        return false;
    }

    // 5. Compute the result based on the input type:
    switch (x.type) {
    case Value::Type::Integer:
        result = Value::make_integer(op(x.integer, y.integer));
        return true;
    case Value::Type::Floating:
        result = Value::make_floating(op(x.floating, y.floating));
        return true;
    default:
        std::cerr << "Non-numeric argument passed to a numeric operator" << std::endl;
        return false;
    }
}

template <class Op>
bool common_numeric_nnary(
        PointerType arguments,
        Machine &machine,
        double neutral,
        Op op,
        Value &result) {

    /*
     * ## Boilerplate necessary for execution of a n-nary numeric operator.
     *
     * This function may handle an arbitrary number of arguments, including
     * zero. This is possible by the fact that we take a neutral element
     * together with the operator to be applied. Let's consider an example
     * of an operator "+" together with a neural value "0" applied to an
     * increasing number of arguments:
     *
     * - (+)       : 0             <- no iterations, neutral "0" returned,
     * - (+ a0)    : 0 + a0        <- a0 + "0" gives a0 by identity,
     * - (+ a0 a1) : (0 + a0) + a1 <- for 2+ arguments we do proper addition.
     * - ...
     *
     * ### Notes
     * - All the operations are performed on an accumulator of type double.
     * - If only integral arguments are passed, the the result is returned as
     *   a truncated integer. Otherwise a floating point result is returned
     *   storing the accumulator value.
     */

    // 1. Initialize the result value:
    double accumulator = neutral;

    // 2. Prepare iteration state:
    bool pure_int = true;
    bool non_numeric = false;

    // 3. Iterate over the input arguments, applying the operator:
    std::for_each(
        ListPool::ConstIterator { machine.pool, arguments }, ListPool::ConstIterator {},
        [&op, &accumulator, &pure_int, &non_numeric](const Value &value) {
            if (non_numeric) return;
            switch (value.type) {
            case Value::Type::Integer:
                accumulator = op(accumulator, value.integer);
                break;
            case Value::Type::Floating:
                accumulator = op(accumulator, value.floating);
                pure_int = false;
                break;
            default:
                non_numeric = true;
                break;
            }
        });

    // 4. Check the error state:
    if (non_numeric) {
        std::cerr << "Non-numeric argument passed to numeric operator" << std::endl;
        return false;
    }

    // 5. Return the result appropriately, depending on whether non-integral
    //    arguments have been encountered:
    result = pure_int ?
        Value::make_integer(accumulator) :
        Value::make_floating(accumulator);
    return true;
}

template <class Op>
bool common_logical_unary(
        PointerType arguments,
        Machine &machine,
        Op op,
        Value &result) {

    /*
     * ## Boilerplate necessary for execution of an unary logical operator.
     */

    // 1. Assert input:
    if (arguments == NIL || machine.pool.cdr(arguments) != NIL) {
        std::cerr << "Invalid number of arguments passed to a unary function" << std::endl;
        return false;
    }

    // 2. Perform the operation according to the input type, or report error if
    //    input of invalid type passed:
    Value argument = machine.pool.car(arguments);
    switch (argument.type) {
    case Value::Type::Boolean:
        result = Value::make_boolean(op(argument.boolean));
        return true;
    default:
        std::cerr << "Non-logical argument passed to a logical function" << std::endl;
        return false;
    }

    // 3. Handle obscure case of "ORing" enums; this should never be done with
    //    Value::type member:
    throw std::runtime_error { "Malformed enum encountered" };
}

template <class Op>
bool common_logical_nnary(
        PointerType arguments,
        Machine &machine,
        double neutral,
        Op op,
        Value &result) {

    /*
     * ## Boilerplate necessary for execution of an n-nary logical operator.
     *
     * For a deeper discussion of the algorithm used here see
     * "common_numeric_nnary".
     */

    // 1. Initialize the result value:
    bool accumulator = neutral;

    // 2. Prepare iteration state:
    bool non_logical = false;

    // 3. Iterate over the input arguments, applying the operator:
    std::for_each(
        ListPool::ConstIterator { machine.pool, arguments }, ListPool::ConstIterator {},
        [&op, &accumulator, &non_logical](const Value &value) {
            if (non_logical) return;
            if (value.type == Value::Type::Boolean)
                accumulator = op(accumulator, value.boolean);
            else
                non_logical = true;
        });

    // 4. Check the error state:
    if (non_logical) {
        std::cerr << "Non-logical argument passed to a logical operator" << std::endl;
        return false;
    }

    // 5. Return the result:
    result = Value::make_boolean(accumulator);
    return true;
}

template <class Op>
bool common_comparison(
        PointerType arguments,
        Machine &machine,
        Op op,
        Value &result) {

    /*
     * ## Boilerplate necessary for execution of a comparison operator.
     *
     * Note, how the implementation of the actual comparison is delegated in the
     * last step to the appropriate operators defined over the "Value" class.
     */

    // 1. Read the arguments, and detect error if their number is incorrect:

    // 1.1. Read the first argument:
    if (arguments == NIL) {
        std::cerr << "Invalid number of arguments passed to a comparison function" << std::endl;
        return false;
    }
    const Value &x = machine.pool.car(arguments);
    arguments = machine.pool.cdr(arguments);

    // 1.2. Read the second argument:
    if (arguments == NIL) {
        std::cerr << "Invalid number of arguments passed to a comparison function" << std::endl;
        return false;
    }
    const Value &y = machine.pool.car(arguments);
    arguments = machine.pool.cdr(arguments);

    // 1.3. Ensure that there are no more arguments:
    if (arguments != NIL) {
        std::cerr << "Invalid number of arguments passed to a comparison function" << std::endl;
        return false;
    }

    // 2. Make sure the arguments' types are equal:
    //    (this is a precondition for some operators)
    if (x.type != y.type) {
        std::cerr << "Comparison function arguments' types mismatch" << std::endl;
        return false;
    }

    // 3. Perform the comparison:
    result = Value::make_boolean(op(x, y));
    return true;
}

bool bif_list(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Evaluates its arguments and returns a list containing the results.
     */

    // 1. Evaluate arguments:
    PointerType arg_values = NIL;
    if (!evaluate_list(machine, arguments, arg_values)) {
        std::cerr << "Failed evaluating arguments for built in function" << std::endl;
        return false;
    }

    // 2. Build list and return it:
    result = Value::make_list(arg_values);
    return true;
}

bool bif_car(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Returns the value from the pointed cons cell.
     */

    // 1. Check the arguments' count:
    if (arguments == NIL) {
        std::cerr << "No arguments passed to \"car\"" << std::endl;
        return false;
    }
    if (machine.pool.cdr(arguments) != NIL) {
        std::cerr << "Too many arguments passed to \"car\"" << std::endl;
        return false;
    }

    // 2. Evaluate the argument:
    const Value &arg_value = machine.pool.car(arguments);
    if (arg_value.type != Value::Type::List) {
        std::cerr << "Ivalid argument to \"car\"" << std::endl;
        return false;
    }

    // 3. Return the requested value:
    result = machine.pool.car(arg_value.list);
    return true;
}

bool bif_cdr(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Returns the "next" pointer from the pointed cell.
     */

    // 1. Check the arguments' count:
    if (arguments == NIL) {
        std::cerr << "No arguments passed to \"cdr\"" << std::endl;
        return false;
    }
    if (machine.pool.cdr(arguments) != NIL) {
        std::cerr << "Too many arguments passed to \"cdr\"" << std::endl;
        return false;
    }

    // 2. Evaluate the argument:
    const Value &arg_value = machine.pool.car(arguments);
    if (arg_value.type != Value::Type::List) {
        std::cerr << "Ivalid argument to \"cdr\"" << std::endl;
        return false;
    }

    // 3. Return the requested value:
    result = Value::make_list(machine.pool.cdr(arg_value.list));
    return true;
}

bool bif_cons(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Constructs a cons cell from two input arguments.
     */

    // 1. Read the head value:
    if (arguments == NIL) {
        std::cerr << "No arguments passed to \"cons\"" << std::endl;
        return false;
    }
    const Value &head_value = machine.pool.car(arguments);
    arguments = machine.pool.cdr(arguments);

    // 2. Read the tail value:
    if (arguments == NIL) {
        std::cerr << "Not enought arguments passed to \"cons\"" << std::endl;
        return false;
    }
    const Value &tail_value = machine.pool.car(arguments);
    if (tail_value.type != Value::Type::List) {
        std::cerr << "Non-list second argument passed to \"cons\"" << std::endl;
        return false;
    }
    arguments = machine.pool.cdr(arguments);

    // 3. Make sure there are no unnecessary values:
    if (arguments != NIL) {
        std::cerr << "Too many arguments passed to \"cons\"" << std::endl;
        return false;
    }

    // 4. Construct the result and report success:
    result = Value::make_list(machine.pool.cons(head_value, tail_value.list));
    return true;
}

// Analytical algorithms
// =====================

bool is_lambda(PointerType ptr,
               const ListPool &pool,
               PointerType &args_list,
               PointerType &body) {

    /*
     * ## Checks if a pointed value is a well formed lambda expression.
     *
     * This function takes a pointer, and checks if it is a well formed lambda
     * expression. If so, pointers to the arguments' list and the body
     * expression are returned through output arguments.
     *
     * ### Expected structure:
     * <lambda> ::= "(" "lambda" <symbol-list> [<expression> [<expression> [...]]] ")"
     *
     * ### Arguments:
     * - ptr:             pointer to the analyzed expression,
     * - pool:            list pool in terms of which the analysis is to be
     *                    performed,
     * - args_list:       (out) the arguments list in the expression,
     * - body:            (out) function body in the expression.
     *
     * ### Returns:
     * - true, if the pointed expression is a well formed lambda,
     * - false otherwise.
     */

    // 1. Assert it's not nil:
    if (ptr == NIL) {
        std::cerr << "nil is not lambda" << std::endl;
        return false;
    }
    const Value &lambda_value = pool.car(ptr);

    // 2. Assert it's a non-empty list:
    if (lambda_value.type != Value::Type::List) {
        std::cerr << "lambda must be a list" << std::endl;
        return false;
    }
    PointerType it = lambda_value.list;
    if (it == NIL) {
        std::cerr << "lambda must not be an empty list" << std::endl;
        return false;
    }

    // 3. Check that the first argument is a proper symbol:
    const Value &symbol_value = pool.car(it);
    if (symbol_value.type != Value::Type::Symbol ||
        symbol_value.symbol != std::string { "lambda" }) {
        std::cerr << "lambda must start with a \"lambda\" symbol" << std::endl;
        return false;
    }
    it = pool.cdr(it);

    // 4. Check that an arguments' list follows:
    if (it == NIL) {
        std::cerr << "Arguments' list missing in lambda" << std::endl;
        return false;
    }
    const Value &arguments_value = pool.car(it);
    if (arguments_value.type != Value::Type::List) {
        std::cerr << "Ill formed arguments' in lambda" << std::endl;
        return false;
    }
    args_list = arguments_value.list;
    if (std::find_if(
            ListPool::ConstIterator { pool, arguments_value.list },
            ListPool::ConstIterator {},
            [](const Value &value) {
                return value.type != Value::Type::Symbol;
            }) != ListPool::ConstIterator {}) {
        std::cerr << "Non-symbol argument found in lambda" << std::endl;
        return false;
    }
    it = pool.cdr(it);

    // 5. Record the lambda body:
    body = it;

    // 6. At last, report success:
    return true;
}

bool is_binding(PointerType ptr,
                const ListPool &pool,
                std::string &key,
                PointerType &value) {

    /*
     * ## Checks if a pointed value is a well formed binding.
     *
     * ### Expected structure:
     * <binding> ::= "(" <symbol> <expression> ")"
     */

    // 1. Assert it's not nil:
    if (ptr == NIL) {
        std::cerr << "nil is not a binding" << std::endl;
        return false;
    }
    const Value &binding_value = pool.car(ptr);

    // 2. Check if the input is a non-empty list:
    if (binding_value.type != Value::Type::List) {
        std::cerr << "Binding must be a list" << std::endl;
        return false;
    }
    PointerType it = binding_value.list;
    if (it == NIL) {
        std::cerr << "Binding must not be an empty list" << std::endl;
        return false;
    }

    // 3. Check if the first argument is a symbol and record it:
    const Value &key_value = pool.car(it);
    if (key_value.type != Value::Type::Symbol) {
        std::cerr << "Binding key must be a symbol" << std::endl;
        return false;
    }
    key = key_value.symbol;
    it = pool.cdr(it);

    // 4. Check if an element follows the symbol, and record it:
    if (it == NIL) {
        std::cerr << "Binding missing the value" << std::endl;
        return false;
    }
    value = it;

    // 5. Check if there are no extra elements in the binding:
    if (pool.cdr(it) != NIL) {
        std::cerr << "Too many elements in a binding list" << std::endl;
        return false;
    }

    // 6. At last, report success:
    return true;
}

template <class Func>
bool is_binding_list(PointerType ptr,
                     const ListPool &pool,
                     Func on_binding) {

    /*
     * ## Checks if a pointed value is a well formed bindings list.
     *
     * ### Expected structure:
     * <binding-list> ::= "(" [<binding> [<binding> [...]]] ")"
     */

    // 1. Assert it's not nil:
    if (ptr == NIL) {
        std::cerr << "nil is not a binding list" << std::endl;
        return false;
    }
    const Value &list_value = pool.car(ptr);

    // 2. Check if it is a list:
    if (list_value.type != Value::Type::List) {
        std::cerr << "Binding list must be a list" << std::endl;
        return false;
    }
    PointerType it = list_value.list;

    // 3. Iterate over the elements, and report them if they are proper bindings:
    while (it != NIL) {
        std::string key;
        PointerType value;
        if (!is_binding(it, pool, key, value)) {
            std::cerr << "Invalid item in a bindings list" << std::endl;
            return false;
        }
        on_binding(key, value);
        it = pool.cdr(it);
    }

    // 4. If we've got here, report success:
    return true;
}

// Execution algorithms
// ====================

template <class BuiltInFunction>
bool execute_built_in_function(BuiltInFunction built_in_function,
                               PointerType args,
                               Machine &machine,
                               Value &result) {
    /*
     * ## Executes a built in function object for a given argument list.
     *
     * The argument's list must first be evaluated, as what is here is a
     * quotation of the arguments in the call.
     *
     * ### Arguments:
     * - built_in_function: the function to be executed,
     * - args:              the list containing the passed argument expressions,
     * - machine:           the machine in terms of which the call is to be
     *                      made,
     * - result:            (out) reference under which the result should be
     *                      written.
     *
     * ### Returns:
     * - true if the execution was successful,
     * - false otherwise.
     */

    // 1. Evaluate arguments:
    PointerType arg_values;
    if (!evaluate_list(machine, args, arg_values)) {
        std::cerr << "Failed evaluating built in function arguments" << std::endl;
        return false;
    }

    // 2. Execute the implementation:
    return built_in_function(arg_values, machine, result);
}

bool execute_function_at(PointerType function_ptr,
                         PointerType actual_args,
                         Machine &machine,
                         Value &result) {

    /*
     * ## Execute a function stored under a speciffic adress.
     */

    // 1. Analyze the pointed structure:
    PointerType formal_args;
    PointerType body;
    if (!is_lambda(function_ptr, machine.pool, formal_args, body)) {
        std::cerr << "Attempted to execute a non-lambda expression" << std::endl;
        return false;
    }

    // 2. Build local scope based on the formal and the actual arguments:
    Scope scope { &machine.global, {} };
    bool error = false;
    PointerType it = formal_args;
    PointerType jt = actual_args;
    ListPool::ConstIterator
        it_first { machine.pool, it }, it_last {},
        jt_first { machine.pool, jt }, jt_last {};
    for_each_2(
        it_first, it_last, jt_first, jt_last,
        [&machine, &scope, &error](const Value &formal, const Value &actual) {
            if (error) return;
            Value value;
            if (!execute_value(actual, machine, value)) {
                std::cerr << "Failed evaluating actual argument" << std::endl;
                error = true;
            } else {
                scope.map[formal.symbol] = machine.pool.cons(value, NIL);
            }
        });
    if (error) return false;
    if (it_first != it_last || jt_first != jt_last) {
        std::cerr << "Formal and actual arguments count mismatch" << std::endl;
        return false;
    }
    machine.local_stack.push_back(scope);

    // 3. Execute the function body:
    while (body != NIL) {
        if (!execute_value(machine.pool.car(body), machine, result)) {
            std::cerr << "Failed executing user defined function" << std::endl;
            machine.local_stack.pop_back(); // <--- ACHTUNG!
            return false;
        }
        body = machine.pool.cdr(body);
    }

    // 4. Destroy the local scope:
    machine.local_stack.pop_back();

    // 5. Report success:
    return true;
}

bool execute_special_quote(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Returns the quoted argument.
     */

    if (arguments == NIL || machine.pool.cdr(arguments) != NIL) {
        std::cerr << "Invalid number of arguments passed to \"quote\"" << std::endl;
        return false;
    }
    result = machine.pool.car(arguments);
    return true;
}

bool execute_special_setq(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Sets a value to a symbol in the current scope.
     *
     * If the symbol is reachable from the current scope, the current value is
     * overwritten. Otherwise a new variable is registered in the current scope.
     *
     * ### Arguments:
     * - arguments: a list of arguments,
     * - machine:   machine in whith the variable should be set,
     * - result:    a side effect of this computation (the assigned value).
     */

    // 1. Read the arguments:

    // 1.1. Evaluate the symbol:
    if (arguments == NIL) {
        std::cerr << "Invalid number of arguments passed to \"setq\"" << std::endl;
        return false;
    }
    const Value &symbol_value = machine.pool.car(arguments);
    if (symbol_value.type != Value::Type::Symbol) {
        std::cerr << "First argument of \"setq\" form must be a symbol" << std::endl;
        return false;
    }
    const std::string &symbol = symbol_value.symbol;
    arguments = machine.pool.cdr(arguments);

    // 1.2. Evaluate the value symbol:
    if (arguments == NIL || machine.pool.cdr(arguments) != NIL) {
        std::cerr << "Invalid number of arguments passed to \"setq\"" << std::endl;
        return false;
    }
    if (!execute_value(machine.pool.car(arguments), machine, result)) {
        std::cerr << "Failed execution of a \"setq\" expression" << std::endl;
        return false;
    }

    // 2. Register the variable:

    // 2.1. Create a new cons cell with the registered value:
    PointerType ptr = machine.pool.cons(result, NIL);

    // 2.2. Register the value in an appropriate location:
    Scope::iterator_type it;
    if (machine.scope_find(symbol, it)) it->second = ptr;
    else machine.scope_set(symbol, ptr);

    // 3. Report success:
    return true;
}

struct GuardedCommandEvaluationResult {
    bool guard_matched;
    bool error_occured;
};

GuardedCommandEvaluationResult try_execute_guarded_command(
        const Value &command, Machine &machine, Value &result) {

    /*
     * ## Tries execute a guarded command.
     *
     * This function takes a value that is expected to be a guarded command and
     * tries to execute it.
     * Guarded command is a list that consists of at least one expression - the
     * guard test -- it must evaluate to a boolean value. The list may contain
     * additional sequence of expressions which are executed should the guard
     * test passes.
     */

    // 1. Assert structure:
    if (command.type != Value::Type::List) {
        std::cerr << "Non-list guarded command" << std::endl;
        return { false, true };
    }

    // 2. Execute the guard:
    PointerType it = command.list;
    Value guard_value;
    if (!execute_value(machine.pool.car(it), machine, guard_value)) {
        std::cerr << "Failed executing a guard" << std::endl;
        return { false, true };
    }
    if (guard_value.type != Value::Type::Boolean) {
        std::cerr << "Non-boolean guard in a guarded command" << std::endl;
        return { false, true };
    }

    // 3.a) Break execution if guard test failed:
    if (!guard_value.boolean) return { false, false };

    // 3.b) Execute the remaining expressions otherwise:
    it = machine.pool.cdr(it);
    while (it != NIL) {
        const Value &current = machine.pool.car(it);
        if (!execute_value(current, machine, result)) {
            std::cerr << "Failed executing a guarded command" << std::endl;
            return { true, true };
        }
        it = machine.pool.cdr(it);
    }

    // 4. Report success:
    return { true, false };
}

bool execute_special_iffi(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Implementation of Dijkstra's if-fi command.
     *
     * This special form consists of the "iffi" keyword and one or more guarded
     * commands.
     * Each guarded command consists of two elements: a guard and an expression.
     * Whenever an iffi special form is executed, one of the commands, for which
     * the guard evaluates to true will be selected, and its expression executed.
     *
     * ### Notes:
     * - For Dijkstra's if-fi, it was not defined, which command is going to be
     *   selected, should more than one guards evaluate to true. Here, the
     *   implementation will always execute the commands in a textual order.
     */

    // 1. Assert input presence:
    if (arguments == NIL) {
        std::cerr << "No guarded commands in \"iffi\"" << std::endl;
        return false;
    }

    // 2. Iterate over the arguments:
    while (arguments != NIL) {

        // 2.1. Try executing the current guarded command, and optionally
        //      break the execution:
        GuardedCommandEvaluationResult gcer = try_execute_guarded_command(
            machine.pool.car(arguments), machine, result);
        if (gcer.error_occured) return false;
        if (gcer.guard_matched) return true;

        // 2.2. Read next command:
        arguments = machine.pool.cdr(arguments);
    }

    // 3. Getting here means that we have failed:
    std::cerr << "No guarded command was executed in \"iffi\"" << std::endl;
    return false;
}

bool execute_special_dood(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Implementation of Dijkstra's do-od command.
     *
     * This special form consists of the "dood" keyword and one or more guarded
     * commands.
     * Each guarded command consists of two elements: a guard and an expression.
     * Whenever a dood special form is executed, its arguments' guards are
     * evaluated one by one, and for each of them for each the guard test passes,
     * the accompanying expression is executed.
     * This happens iteratively as long as at least one guard test has passed.
     */

    // 1. Assert input presence:
    if (arguments == NIL) {
        std::cerr << "No guarded commands in \"dood\"" << std::endl;
        return false;
    }

    // 2. Prepare for no command being evaluated:
    result = Value::make_list(NIL);

    // 3. Begin the main iteration:
    bool carry_on;
    do {
        // 3.1. Prepare for the loop break:
        carry_on = false;

        // 3.2. Iterate over the guarded commands:
        PointerType it = arguments;
        while (it != NIL) {

            // 3.2.1. Try executing the current guarded command:
            GuardedCommandEvaluationResult gcer = try_execute_guarded_command(
                machine.pool.car(it), machine, result);

            // 3.2.2. Break the execution on error:
            if (gcer.error_occured) return false;

            // 3.2.3. Prevent loop break if guard matched:
            if (gcer.guard_matched) carry_on = true;

            // 3.2.4. Read next command:
            it = machine.pool.cdr(it);
        }

    } while (carry_on);

    // 4. If we have got here, report success:
    return true;
}

bool execute_special_defun(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Execute a defun special form.
     *
     * Defun must contain:
     * 1. a name under which the function is to be stored,
     * 2. a formal argument list with the argument names,
     * 3. the function body.
     */

    // 0. Check if any arguments passed:
    if (arguments == NIL) {
        std::cerr << "No arguments passed to \"defun\"" << std::endl;
        return false;
    }

    // 1. Read the function name:
    const Value &name_value = machine.pool.car(arguments);
    // 1.1. Assert the element's properties:
    if (name_value.type != Value::Type::Symbol) {
        std::cerr << "Failed reading name in \"defun\"" << std::endl;
        return false;
    }
    // 1.2. Read next element:
    arguments = machine.pool.cdr(arguments);
    if (arguments == NIL) {
        std::cerr << "Not enough arguments passed to \"defun\"" << std::endl;
        return false;
    }

    // 2. Read the formal arguments' list:
    const Value &args_value = machine.pool.car(arguments);
    // 2.1. Check if the second element is a list:
    if (args_value.type != Value::Type::List) {
        std::cerr << "Failed reading arguments in \"defun\"" << std::endl;
        return false;
    }
    // 2.2. Check if all the formal arguments are symbols:
    PointerType args_list = args_value.list;
    while (args_list != NIL) {
        const Value &arg_value = machine.pool.car(args_list);
        if (arg_value.type != Value::Type::Symbol) {
            std::cerr << "Non-symbol argument found in \"defun\"" << std::endl;
            return false;
        }
        args_list = machine.pool.cdr(args_list);
    }
    // 2.3. Read next element:
    arguments = machine.pool.cdr(arguments);
    if (arguments == NIL) {
        std::cerr << "Not enough arguments passed to \"defun\"" << std::endl;
        return false;
    }

    // 3. Read the function's body:
    const Value &body_value = machine.pool.car(arguments);

    // 4. Check that no extra elements were given:
    if (machine.pool.cdr(arguments) != NIL) {
        std::cerr << "Too many arguments passed to \"defun\"" << std::endl;
        return false;
    }

    // 5. Build and store the function object:
    PointerType result_elements = NIL;
    result_elements = machine.pool.cons(body_value, result_elements);
    result_elements = machine.pool.cons(args_value, result_elements);
    result_elements = machine.pool.cons(Value::make_symbol("lambda"), result_elements);
    result = Value::make_list(result_elements);
    machine.scope_set(name_value.symbol, machine.pool.cons(result, NIL));
    return true;
}

bool execute_special_let(PointerType arguments, Machine &machine, Value &result) {

    /*
     * ## Implementation of Lisp's let special form.
     *
     * "Let" creates a local scope in which one or more commands are executed.
     * The form must contain a (potentially empty) list of bindings that are
     * going to be available for the executed commands, and one or more command.
     */

    // 0. Setup for binding registration callbacks:
    std::vector<std::pair<std::string, PointerType>> bindings;
    auto on_binding = [&bindings](const std::string &key, PointerType value) {
        bindings.push_back(std::make_pair(key, value));
    };

    // 1. Assert input presence:
    if (arguments == NIL) {
        std::cerr << "No bindings in \"let\"" << std::endl;
        return false;
    }

    // 2. Read the bindings list:
    if (!is_binding_list(arguments, machine.pool, on_binding)) {
        std::cerr << "Failed reading bindings list in \"let\"" << std::endl;
        return false;
    }
    arguments = machine.pool.cdr(arguments);

    // 3. Assert there is at least one command:
    if (arguments == NIL) {
        std::cerr << "No command provided in \"let\"" << std::endl;
        return false;
    }

    // 4. Build a local scope:
    Scope scope {
        machine.local_stack.empty() ?
            &machine.global :
            &machine.local_stack.back(),
        {}
    };
    for (const std::pair<std::string, PointerType> &binding : bindings)
        scope.map[binding.first] = binding.second;
    machine.local_stack.push_back(scope);

    // 4. Execute the provided commands:
    while (arguments != NIL) {
        if (!execute_value(machine.pool.car(arguments), machine, result)) {
            std::cerr << "Failed executing an expression inside \"let\""
                      << std::endl;
            machine.local_stack.pop_back(); // <--- ACHTUNG!
            return false;
        }
        arguments = machine.pool.cdr(arguments);
    }

    // 5. Clean up an return success:
    machine.local_stack.pop_back();
    return true;
}

bool execute_call(PointerType list, Machine &machine, Value &result) {

    /*
     * ## Execute call encoded as a list:
     *
     * ### Arguments:
     * - list:    the list representing the call to be made,
     * - machine: the machine in terms of which the call is to be made,
     * - result:  (out) reference under which the result should be written.
     *
     * ### Returns:
     * - true if the execution was successful,
     * - false otherwise.
     *
     * ### Preconditions:
     * - list != NIL,
     */

    // 0. Check Preconditions:
    assert(list != NIL);

    // 1. Take the first list element, and ensure it is a symbol:
    Value head = machine.pool.car(list);
    PointerType tail = machine.pool.cdr(list);
    if (head.type != Value::Type::Symbol) {
        std::cerr << "Invalid first element type in a call: "
                  << to_string(head, machine.pool)
                  << std::endl;
        return false;
    }
    const std::string &symbol = head.symbol;

    // 2. Try execute the form in one of possible ways:

    // 2.1. Try executing as a built in function:
    auto it = machine.built_in_functions.find(symbol);
    if (it != machine.built_in_functions.end())
        return execute_built_in_function(it->second, tail, machine, result);

    // 2.2. Try executing function stored in the machine:
    PointerType ptr;
    if (machine.scope_get(symbol, ptr))
        return execute_function_at(ptr, tail, machine, result);

    // 2.3. Try executing a special form:
    if (symbol == "quote") return execute_special_quote(tail, machine, result);
    if (symbol == "setq")  return execute_special_setq(tail, machine, result);
    if (symbol == "iffi")  return execute_special_iffi(tail, machine, result);
    if (symbol == "dood")  return execute_special_dood(tail, machine, result);
    if (symbol == "defun") return execute_special_defun(tail, machine, result);
    if (symbol == "let")   return execute_special_let(tail, machine, result);

    // 2.4. If got here, report failure:
    std::cerr << "Unknown function or special form: \"" << symbol
              << "\"" << std::endl;
    return false;
}

bool test_exec_sequence(const std::string &source,
                           const std::vector<Value> &expected_results,
                           Machine &machine) {

    // 1. Define the input and expected results' ranges:
    auto first = source.begin(), last = source.end();
    auto expected_first = expected_results.begin();
    auto expected_last = expected_results.end();

    // 2. Parse source expression by expression:
    while (first != last && expected_first != expected_last) {

        // 2.1. Execute next actual expression:
        Value actual_result;
        if (!execute_source(first, last, machine, actual_result)) {
            std::cerr << "Failed executing source: " << source << std::endl;
            return false;
        }

        // 2.2. Compare against next expected result:
        if (actual_result != *expected_first++) {
            std::cerr <<
                "Actual value not equal to expected after executing source: " <<
                source <<
                std::endl;
            return false;
        }
    }

    // 3. Check that both the input and the expected values range have been
    //    depleted:
    if (first != last || expected_first != expected_last) {
        std::cerr << "Incorrect number of results in source: " << source << std::endl;
        return false;
    }

    // 4. If we have got this far, report success:
    return true;
}

bool test_exec_one(const std::string &source,
                      const Value &expected_result,
                      Machine &machine) {
    return test_exec_sequence(source, { expected_result }, machine);
}

bool test_exec_progn(const std::string &source,
                        const Value &expected_result,
                        Machine &machine) {

    // 1. Define the input range:
    auto first = source.begin(), last = source.end();

    // 2. Execute source until depletion or an error occurs:
    Value last_result;
    while (first != last) {
        if (!execute_source(first, last, machine, last_result)) {
            std::cerr << "Failed executing source: " << source << std::endl;
            return false;
        }
    }

    // 3. Assert correct result:
    if (last_result != expected_result) {
        std::cerr << "Failed function test: " << source << std::endl;
        std::cerr << "Expected: " << to_string(expected_result, machine.pool) << std::endl;
        std::cerr << "Actual: " << to_string(last_result, machine.pool) << std::endl;
        return false;
    }

    // 4. If we have got this far, report success:
    return true;
}

}

// Public functions
// ================

void register_default_built_in_functions(Machine &machine) {

    /*
     * ## Registers default built in functions in the given machine.
     */

    // 1. Arythmetic operators:
    machine.built_in_functions["+"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_numeric_nnary(arguments, machine, 0, std::plus<double> {}, result);
    };
    machine.built_in_functions["*"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_numeric_nnary(arguments, machine, 1, std::multiplies<double> {}, result);
    };
    machine.built_in_functions["%"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_numeric_binary(arguments, machine, std::modulus<int> {}, result);
    };
    machine.built_in_functions["inv"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_numeric_unary(arguments, machine, invert<double> {}, result);
    };

    // 2. Logical operators:
    machine.built_in_functions["and"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_logical_nnary(arguments, machine, true, std::logical_and<bool> {}, result);
    };
    machine.built_in_functions["or"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_logical_nnary(arguments, machine, false, std::logical_or<bool> {}, result);
    };
    machine.built_in_functions["not"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_logical_unary(arguments, machine, std::logical_not<bool> {}, result);
    };

    // 3. Comparison operators:
    machine.built_in_functions["="] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_comparison(arguments, machine, ValueCompare { machine.pool }, result);
    };
    machine.built_in_functions["/="] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_comparison(arguments, machine, std::not_equal_to<Value> {}, result);
    };
    machine.built_in_functions["<"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_comparison(arguments, machine, std::less<Value> {}, result);
    };
    machine.built_in_functions[">"] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_comparison(arguments, machine, std::greater<Value> {}, result);
    };
    machine.built_in_functions["<="] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_comparison(arguments, machine, std::less_equal<Value> {}, result);
    };
    machine.built_in_functions[">="] = [](PointerType arguments, Machine &machine, Value &result) {
        return common_comparison(arguments, machine, std::greater_equal<Value> {}, result);
    };

    // 4. List operators:
    machine.built_in_functions["list"] = bif_list;
    machine.built_in_functions["car"] = bif_car;
    machine.built_in_functions["cdr"] = bif_cdr;
    machine.built_in_functions["cons"] = bif_cons;
}

bool execute_value(const Value &value, Machine &machine, Value &result) {

    /*
     * ## Executes a data structure stored in a lisp value.
     *
     * The following cases of the input value are considered:
     * 1. A symbol, which results in returning the result of its dereferencing,
     * 2. a literal value, which results in returning the value itself,
     * 3. a list, which if not nil is interpreted as a function call, otherwise
     *    a nil value is returned.
     *
     * ### Arguments:
     * - value:   the value to be executed,
     * - machine: the machine in terms of which the execution happens,
     * - result:  (out) a variable to store the result.
     *
     * ### Returns:
     * - true if the execution was successful,
     * - false otherwise.
     */

    ExecutionScopeDebugger scope_debugger(
        "execute_value",
        to_string(value, machine.pool),
        result,
        machine);

    // 1. Execute value in a way that depends on its type:
    switch (value.type) {
    case Value::Type::Symbol:
    {
        // 1.a) Symbol means a reference to a symbol table:
        PointerType ptr;
        if (machine.scope_get(value.symbol, ptr)) {
            result = machine.pool.car(ptr);
            return true;
        } else {
            std::cerr << "Symbol: \"" << value.symbol << "\" not found" << std::endl;
            return false;
        }
    }
    case Value::Type::Boolean:
    case Value::Type::Integer:
    case Value::Type::Floating:
        // 1.b) and 1.c) Numeric values just get copied to output:
        result = value;
        return true;
    case Value::Type::List:
        // 2.d) A list value means EITHER nil OR a call:
        if (value.list == NIL) {
            result = value;
            return true;
        } else {
            return execute_call(value.list, machine, result);
        }
    case Value::Type::Sentinel:
        throw std::runtime_error { "Sentinels illegal in this type of machine" };
    }

    // 2. Handle obscure case of "ORing" enums; this should never be done with
    //    Value::type member:
    throw std::runtime_error { "Malformed enum encountered" };
}

bool test_execution() {

    // 1. Initialize the system under test:
    Machine machine;
    register_default_built_in_functions(machine);

    // 2. Perform tests:
    return
        // 2.1. Execution of simple expressions:
        test_exec_one("1", Value::make_integer(1), machine) &&
        test_exec_one("2.0", Value::make_floating(2), machine) &&
        test_exec_one("nil", Value::make_list(NIL), machine) &&
        test_exec_one("()", Value::make_list(NIL), machine) &&

        // 2.2. Quoting:
        test_exec_one("(quote sym)", Value::make_symbol("sym"), machine) &&
        test_exec_one("'sym", Value::make_symbol("sym"), machine) &&

        // 2.3. Some built in functions test:
        // 2.3.1. Arythmetic:
        test_exec_one("(* 2 (+ 2 2))", Value::make_integer(8), machine) &&
        test_exec_one("(+ (* 2 2) 2)", Value::make_integer(6), machine) &&
        test_exec_one("(inv -2.0)", Value::make_floating(-0.5), machine) &&
        test_exec_one("(% 0 3)", Value::make_integer(0), machine) &&
        test_exec_one("(% 1 3)", Value::make_integer(1), machine) &&
        test_exec_one("(% 2 3)", Value::make_integer(2), machine) &&
        test_exec_one("(% 3 3)", Value::make_integer(0), machine) &&
        // 2.3.2. Logical:
        test_exec_one("(and)", Value::make_boolean(1), machine) &&
        test_exec_one("(and false false)", Value::make_boolean(0), machine) &&
        test_exec_one("(and false true)", Value::make_boolean(0), machine) &&
        test_exec_one("(and true false)", Value::make_boolean(0), machine) &&
        test_exec_one("(and true true)", Value::make_boolean(1), machine) &&
        test_exec_one("(or)", Value::make_boolean(0), machine) &&
        test_exec_one("(or false false)", Value::make_boolean(0), machine) &&
        test_exec_one("(or false true)", Value::make_boolean(1), machine) &&
        test_exec_one("(or true false)", Value::make_boolean(1), machine) &&
        test_exec_one("(or true true)", Value::make_boolean(1), machine) &&
        test_exec_one("(not true)", Value::make_boolean(0), machine) &&
        test_exec_one("(not false)", Value::make_boolean(1), machine) &&
        // 2.3.3. Comparison:
        test_exec_one("(< 1 2)", Value::make_boolean(1), machine) &&
        test_exec_one("(< 2 1)", Value::make_boolean(0), machine) &&
        test_exec_one("(< 2 2)", Value::make_boolean(0), machine) &&
        test_exec_one("(<= 2 2)", Value::make_boolean(1), machine) &&
        test_exec_one("(>= 2 2)", Value::make_boolean(1), machine) &&
        test_exec_one("(= 2 1)", Value::make_boolean(0), machine) &&
        test_exec_one("(= 2 2)", Value::make_boolean(1), machine) &&
        test_exec_one("(/= 2 1)", Value::make_boolean(1), machine) &&
        test_exec_one("(/= 2 2)", Value::make_boolean(0), machine) &&
        test_exec_one("(= 'symbol-1 'symbol-1)", Value::make_boolean(1), machine) &&
        test_exec_one("(= 'symbol-1 'symbol-2)", Value::make_boolean(0), machine) &&
        test_exec_one("(< 'a-symbol 'b-symbol)", Value::make_boolean(1), machine) &&
        test_exec_one("(< 'b-symbol 'a-symbol)", Value::make_boolean(0), machine) &&
        // 2.3.4. List-related:
        test_exec_progn(
            "(setq foo (list 1 2 3))"
            "(* (car foo) (car (cdr foo)) (car (cdr (cdr foo))))",
            Value::make_integer(6),
            machine) &&
        test_exec_progn(
            "(setq foo (cons 1 (cons 2 (cons 3 nil))))"
            "(+ (car foo) (car (cdr foo)) (car (cdr (cdr foo))))",
            Value::make_integer(6),
            machine) &&

        // 2.4. Global scope manipulation:
        test_exec_sequence(
            "(setq foo 2) foo",
            {
                Value::make_integer(2),
                Value::make_integer(2)
            },
            machine) &&

        // 2.5. Control expressions:
        test_exec_one("(iffi (true 1.0) (false 2))", Value::make_floating(1.0), machine) &&
        test_exec_one("(iffi (false 1.0) (true 2))", Value::make_integer(2), machine) &&
        test_exec_sequence(
            "(setq i 0)"
            "(dood ((/= i 10) (setq i (+ i 1))))"
            "i",
            {
                Value::make_integer(0),
                Value::make_integer(10),
                Value::make_integer(10),
            },
            machine) &&

        // 2.6. let:
        test_exec_progn(
            "(setq x 1)"
            "(setq y 2)"
            "(let ((x 5)) (setq y (+ y x)))"
            "y",
            Value::make_integer(7),
            machine) &&

        // 2.7. Functions:
        test_exec_progn(
            "(defun sqr (x) (* x x))(sqr 3.0)",
            Value::make_floating(9),
            machine) &&
        test_exec_progn(
            "(setq bar '(lambda (x y) (+ x y)))(bar 1 2)",
            Value::make_integer(3),
            machine) &&
        test_exec_progn(
            "(defun gcd (x y)"
            "    (dood ((/= y 0)"
            "        (setq t y)"
            "        (setq y (% x y))"
            "        (setq x t))))"
            "(gcd 42 56)",
            Value::make_integer(14),
            machine);
}
