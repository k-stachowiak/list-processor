#ifndef ALGORITHM_HPP
#define ALGORITHM_HPP

template <class It, class Func>
Func for_each_2(It &it_first, It it_last, It &jt_first, It jt_last, Func func) {

    /*
     * ## Calls a given function for each pair of elements taken from two lists.
     *
     * If this function is called with two lists:
     * - a0, a1, a2, ...,
     * - b0, b1, b2, ...,
     * then the given function will be called with the following pairs of
     * elements: (a0, b0), (a1, b1), (a2, b2), ...
     *
     * The "first" iterators are taken by reference and are _modified_ in the
     * course of the algorithm, so that afterwards it may be checked e.g. if
     * both are equal to respective "last" iterators, which means that both
     * lists were exhausted in the interation, i.e. both had the same element
     * count.
     *
     * ### Template arguments:
     * - It:   the iterator type
     * - Func: function object: (const It::value_type&, const It::value_type&) -> void
     */

    while (it_first != it_last && jt_first != jt_last) {
        func(*it_first, *jt_first);
        ++it_first;
        ++jt_first;
    }
    return func;
}

#endif
