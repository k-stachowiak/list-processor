#ifndef PARSE_HPP
#define PARSE_HPP

#include "config.hpp"
#include "data.hpp"
#include "tokenize.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

#include <cassert>

template <class It>
struct TrackingIterator : std::iterator<std::forward_iterator_tag, char> {

    /*
     * ## Iterator that tracks lines and columns as it reads subsequent
     *    characters.
     *
     * ### Template arguments:
     * - It: the underlying iterator, which must be a forward iterator over type
     *       char.
     */

    It impl;
    int line, column;

    TrackingIterator(const It &impl) : impl { impl }, line { 1 }, column { 1 } {}

    // TrackingIterator is semi-regular:
    ~TrackingIterator() = default;
    TrackingIterator() : impl {}, line { -1 }, column { -1 } {}
    TrackingIterator(const TrackingIterator& x) = default;
    TrackingIterator &operator=(const TrackingIterator &x) = default;

    // TrackingIterator is regular (and equality comparable):
    friend bool operator==(const TrackingIterator &x, const TrackingIterator &y) {
        return x.impl == y.impl;
    }

    friend bool operator!=(const TrackingIterator &x, const TrackingIterator &y) {
        return !(x == y);
    }

    // TrackingIterator is an Iterator:
    TrackingIterator &operator++() {
        if (*impl == '\n') {
            ++line;
            column = 1;
        } else {
            ++column;
        }
        ++impl;
        return *this;
    }

    TrackingIterator operator++(int) {
        TrackingIterator copy = *this;
        operator++();
        return copy;
    }

    const char &operator*() const { return *impl; }

    // TrackingIterator is an input iterator:
    const Value *operator->() {
        return std::addressof(operator*());
    }
};

/*
 * ## Access a list and column for TrackingIterator.
 *
 * The two functions below provide implementation for the TrackingIterator,
 * which is, by design, capable of providing the information.
 */

template <class It>
int line(const TrackingIterator<It> &x) { return x.line; }

template <class It>
int column(const TrackingIterator<It> &x) { return x.column; }

/*
 * ## Access a list and column for any other iterator.
 *
 * The line and column information aren't needed for the algorithms' correctness
 * therefore, for certain tasks, such as automated testing, we may use iterators
 * that don't provide it, and still have a working parser. The following
 * functions provide the dummy line and column access implementation, that
 * enables using any forward character iterator in the parser.
 */

template <class T>
int line(const T &) { return -1; }

template <class T>
int column(const T &) { return -1; }

/*
 * ## Common BNF rules:
 *
 * <expression>  ::= <atom>|<list>
 * <atom>        ::= <numeric>|<special>|<symbol>
 * <numeric>     ::= <integral>|<floating>
 * <integral>    ::= [<sign>]<digit>|<integral><digit>
 * <floating>    ::= [<sign>]<integral> "." <integral>
 * <sign>        ::= "+"|"-"
 * <special>     ::= "true" | "false" | "nil"
 * <list>        ::= "(" [<expression> [<expression> [...]]] ")"
 * <symbol-list> ::= "(" [<symbol> [<symbol> [...]]] ")"
 * <symbol>      ::= This may be a problem, but it is not easily defined other
 *                   than being a string that is not a special, not a number,
 *                   and doesn't contain white spaces.
 */

Value parse_atom(const std::string &x);

template <class It>
bool parse_expression(It &first, It last, ListPool &pool, Value &result);

template <class It>
bool parse_list(It &first, It last, ListPool &pool, Value &result) {

    /*
     * ## Parses a list from a range of characters.
     *
     * ### Preconditions:
     * 1. first != last,
     * 2. at least one token is in the range.
     */

    // 0. Check Preconditions:
    first = std::find_if_not(first, last, isspace);
    assert(first != last);

    // 1. Initialize result list:
    PointerType list = NIL;

    // 2. Iterate over list elements:
    It first_list_copy = first;
    while (first != last) {

        // 2.1. Look-ahead check for list closing:
        //     (if the look-ahead succeeds, then we must actually consume the
        //      closing parenthesis token, so that we don't parse it again)
        if (*first == ')') {
            result = Value::make_list(pool.reverse(list));
            std::string _ = next_token(first, last);
            return true;
        }

        // 2.2. If list not closed, then parse next expression:
        Value sub_expression;
        It first_element_copy = first;
        if (!parse_expression(first, last, pool, sub_expression)) {
            std::cerr << "Failed parsing list element at ("
                      << line(first_element_copy) << ", "
                      << column(first_element_copy) << ")"
                      << std::endl;
            return false;
        } else {
            list = pool.cons(sub_expression, list);
        }
    }

    // 3. Handle end of input while awaiting closing parenthesis:
    std::cerr << "Unexpected end of source while parsing list started at ("
              << line(first_list_copy) << ", "
              << column(first_list_copy) << ")"
              << std::endl;
    return false;
}

template <class It>
bool parse_expression(It &first, It last, ListPool &pool, Value &result) {

    /*
     * ## Reads characters from a range, and builds their lisp representation.
     *
     * ### Arguments:
     * - first:  The beginning of the analyzed range,
     * - last :  the end of the analyzed range,
     * - pool :  the pool in which the temporary and the ultimate data is to be
     *           stored,
     * - result: the value holding the parsed expression.
     *
     * ### Returns:
     * - true if the parsing was successful,
     * - false otherwise.
     *
     * ### Preconditions:
     * 1. source is not empty, and contains at least one non-whitespace
     *    character.
     *
     * ### Postconditions:
     * - If true has been returned the result variable holds the parsing result.
     * - if true was returned, then:
     *   - EITHER first == last,
     *   - OR     first != last AND *first is not a white space.
     *
     */

    // 0. Check Preconditions:
    first = std::find_if_not(first, last, isspace);
    assert(first != last);

    // 1. Consume next token:
    //    (call to next_token secures the Postconditions)
    std::string token = next_token(first, last);

    // 2. Parser dispatch:
    if (token == "(") {
        // 2.a) If opening parenthesis, we consume a list:
        return parse_list(first, last, pool, result);
    } else if (token == "'") {
        // 2.b) If quote operator, then quote the next expression:
        Value quoted;
        if (!parse_expression(first, last, pool, quoted)) {
            std::cerr << "Failed parsing of a quoted expression" << std::endl;
            return false;
        }
        PointerType quote = NIL;
        quote = pool.cons(quoted, quote);
        quote = pool.cons(Value::make_symbol("quote"), quote);
        result = Value::make_list(quote);
        return true;
    } else {
        // 2.c) We encountered a single atom; parse it:
        result = parse_atom(token);
        return true;
    }
}

bool test_parser();

/*
 * Let's place the to_string algorithms in this module, as they are inverse
 * of parsing.
 */

std::string to_string(const std::vector<Value> &values, const ListPool &pool);
std::string to_string(const Value &x, const ListPool &pool);

#endif
