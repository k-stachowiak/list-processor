#ifndef DATA_HPP
#define DATA_HPP

#include "config.hpp"

#include <vector>
#include <utility>
#include <iterator>
#include <memory>
#include <stdexcept>

#include <cassert>
#include <cstring>

char *string_copy(const char *x);

struct Value {

    /*
     * ## A representation of a boxed value of one of a set of types.
     */

    enum Type { Symbol, Boolean, Integer, Floating, List, Sentinel } type;

    union {
        char *symbol;
        char boolean;
        int integer;
        double floating;
        PointerType list;
        PointerType sentinel_address;
    };

    // Value is semi-regular:
    Value() : type { Type::List }, list { NIL } {}
    Value(const Value &x) : type { x.type } {
        switch (type) {
        case Type::Symbol:
            symbol = string_copy(x.symbol);
            break;
        case Type::Boolean:
            boolean = x.boolean;
            break;
        case Type::Integer:
            integer = x.integer;
            break;
        case Type::Floating:
            floating = x.floating;
            break;
        case Type::List:
            list = x.list;
            break;
        case Type::Sentinel:
            sentinel_address = x.sentinel_address;
            break;
        }
    }
    Value(Value &&x) : type { x.type } {
        switch (type) {
        case Type::Symbol:
            symbol = x.symbol;
            x.symbol = nullptr;
            break;
        case Type::Boolean:
            boolean = x.boolean;
            break;
        case Type::Integer:
            integer = x.integer;
            break;
        case Type::Floating:
            floating = x.floating;
            break;
        case Type::List:
            list = x.list;
            break;
        case Type::Sentinel:
            sentinel_address = x.sentinel_address;
            break;
        }
    }
    ~Value() {
        if (type == Type::Symbol)
            delete [] symbol;
    }
    Value &operator=(const Value &x) {
        if (type == Type::Symbol)
            delete [] symbol;
        type = x.type;
        switch (type) {
        case Type::Symbol:
            symbol = string_copy(x.symbol);
            break;
        case Type::Boolean:
            boolean = x.boolean;
            break;
        case Type::Integer:
            integer = x.integer;
            break;
        case Type::Floating:
            floating = x.floating;
            break;
        case Type::List:
            list = x.list;
            break;
        case Type::Sentinel:
            sentinel_address = x.sentinel_address;
            break;
        }
        return *this;
    }
    Value &operator=(Value &&x) {
        if (type == Type::Symbol)
            delete [] symbol;
        type = x.type;
        switch (type) {
        case Type::Symbol:
            symbol = x.symbol;
            x.symbol = nullptr;
            break;
        case Type::Boolean:
            boolean = x.boolean;
            break;
        case Type::Integer:
            integer = x.integer;
            break;
        case Type::Floating:
            floating = x.floating;
            break;
        case Type::List:
            list = x.list;
            break;
        case Type::Sentinel:
            sentinel_address = x.sentinel_address;
            break;
        }
        return *this;
    }

    // Value is regular*:
    friend bool operator==(const Value &x, const Value &y) {
        if (x.type != y.type) return false;
        switch (x.type) {
        case Type::Symbol: return std::strcmp(x.symbol, y.symbol) == 0;
        case Type::Boolean: return x.boolean == y.boolean;
        case Type::Integer: return x.integer == y.integer;
        case Type::Floating: return x.floating == y.floating;
        case Type::List: return x.list == y.list;
        case Type::Sentinel: return x.sentinel_address == y.sentinel_address;
        }
        throw std::runtime_error { "Malformed enum encountered" };
    }
    friend bool operator!=(const Value &x, const Value &y) { return !(x == y); }

    // Value is totally ordered*:
    friend bool operator<(const Value &x, const Value &y) {

        /*
         * ## Less than comparisong operator.
         *
         * ### Preconditions:
         * - x.type == y.type
         */

        // 0. Assert the preconditions:
        assert(x.type == y.type);

        // 1. Perform the comparison:
        switch (x.type) {
        case Type::Symbol: return std::strcmp(x.symbol, y.symbol) < 0;
        case Type::Boolean: return !!x.boolean < !!y.boolean;
        case Type::Integer: return x.integer < y.integer;
        case Type::Floating: return x.floating < y.floating;
        case Type::List: return x.list < y.list;
        case Type::Sentinel: return x.sentinel_address < y.sentinel_address;
        }

        // 2. Handle obscure case of "ORing" enums; this should never be done
        // with Value::type member:
        throw std::runtime_error { "Malformed enum encountered" };
    }
    friend bool operator>(const Value &x, const Value &y) { return y < x; }
    friend bool operator<=(const Value &x, const Value &y) { return !(x > y); }
    friend bool operator>=(const Value &x, const Value &y) { return !(x < y); }

    // * We want this type to be regular and totally ordered, so that we can
    //   cleanly implement the machine's operations using the implementation
    //   that can be naturally placed inside the class.

    // Value is constructed via static constructor functions. This is not done
    // via a set of appropriate constructors, as the unavoidable implicit
    // conversions (e.g. between char and int) would make it impossible to have
    // the constructors called conveniently.

    static Value make_symbol(const std::string &x) {
        Value result;
        result.type = Type::Symbol;
        result.symbol = string_copy(x.c_str());
        return result;
    }

    static Value make_boolean(char x) {
        Value result;
        result.type = Type::Boolean;
        result.boolean = x;
        return result;
    }

    static Value make_integer(int x) {
        Value result;
        result.type = Type::Integer;
        result.integer = x;
        return result;
    }

    static Value make_floating(double x) {
        Value result;
        result.type = Type::Floating;
        result.floating = x;
        return result;
    }

    static Value make_list(PointerType x) {
        Value result;
        result.type = Type::List;
        result.list = x;
        return result;
    }

    static Value make_sentinel(PointerType x) {
        Value result;
        result.type = Type::Sentinel;
        result.sentinel_address = x;
        return result;
    }
};

bool test_value();

class ListPool {

    /*
     * ## A memory pool of Lisp-like lists.
     */

    std::vector<std::pair<Value, PointerType>> impl;
    PointerType garbage = NIL;

    // Not sure if this can be done a cleaner way...
    friend bool test_list_pool_basics();
    friend bool test_list_pool_reverse();

public:
    void clear() {
        impl.clear();
        garbage = NIL;
    }

    Value &car(PointerType pointer) {

        /*
         * ### Returns the value in the cons cell pointed by the given address.
         *
         * #### Preconditions:
         * 1. Pointer is not nil,
         * 2. Pool contains at least (size_t)pointer cells.
         */

        // 0. Check Preconditions:
        assert(pointer != NIL);
        assert(static_cast<decltype(impl)::size_type>(pointer) <= impl.size());

        // 1. Return the requested value:
        return impl[pointer - 1].first;
    }

    const Value &car(PointerType pointer) const {
        return impl[pointer - 1].first;
    }

    PointerType &cdr(PointerType pointer) {

        /*
         * ### Returns the pointer to the next cell for the given cons cell
         *     pointer.
         *
         * #### Preconditions:
         * 1. Pointer is not nil,
         * 2. Pool contains at least (size_t)pointer cells.
         *
         * #### Postconditions:
         * 1.a) EITHER result is nil,
         * 1.b) OR     result is a valid pointer in terms of this list.
         */

        // 0. Check Preconditions:
        assert(pointer != NIL);
        assert(static_cast<decltype(impl)::size_type>(pointer) <= impl.size());

        // 1. Return the requested pointer:
        return impl[pointer - 1].second;
    }

    const PointerType &cdr(PointerType pointer) const {
        return impl[pointer - 1].second;
    }

    PointerType cons(Value head, PointerType tail) {

        /*
         * ### Create a cons cell.
         *
         * #### Notes:
         * - head is passed by value intentionally. If it was a (const)
         *   reference, it is quite likely it would have pointed to an element
         *   inside impl. However, here we modify impl, which will in many cases
         *   cause impl to reallocate, invalidating iterators, but also plain
         *   pointers. E.g. in a call of a form:
         *
         *       pool.cons(pool.car(ptr), NIL);
         *
         *   note, how car(ptr), being a reference, may become invalid upon the
         *   call to std::vector::push_back() below.
         *   Besides, we can move it into the result anyway, eliding the
         *   additional copy.
         */

        PointerType result;
        if (garbage == NIL) {
            impl.push_back({});
            result = impl.size();
        } else {
            result = garbage;
            garbage = cdr(garbage);
        }
        car(result) = std::move(head);
        cdr(result) = tail;
        return result;
    }

    PointerType length(PointerType list) {

        /*
         * ### Returns a list length.
         */

        PointerType result = 0;
        while (list != NIL) {
            ++result;
            list = cdr(list);
        }
        return result;
    }

    PointerType release(PointerType pointer) {

        /*
         * ### Releases a given cons cell for further use.
         *
         * #### Preconditions:
         * 1. Pointer is not nil,
         * 2. Pool contains at least (size_t)pointer cells.
         */

        // 0. Check Preconditions:
        assert(pointer != NIL);
        assert(static_cast<decltype(impl)::size_type>(pointer) <= impl.size());

        // 1. Store the rest of the list:
        PointerType tail = cdr(pointer);

        // 2. Put the released cell in the gargabe list:
        cdr(pointer) = garbage;
        garbage = pointer;

        // 3. Return the stored rest of the list:
        return tail;
    }

    PointerType reverse(PointerType pointer) {

        /*
         * ### Reverses a list given by a pointer in-place.
         *
         * The old list pointer is no longer useful.
         *
         * #### Preconditions:
         * 1.a) EITHER pointer = NIL,
         * 1.b) OR     pool contains at least (size_t)pointer cells.

         * #### Postconditions:
         * 1.a) EITHER result is nil,
         * 1.b) OR     result is a valid pointer in terms of this list.
         */

        // 0. Check preconditions:
        assert(pointer == NIL ||
               static_cast<decltype(impl)::size_type>(pointer) <= impl.size());

        // 1. Initialize the result:
        PointerType result = NIL;

        // 2. Relink the "next" pointers in the input list:
        while (pointer) {
            PointerType next_argument = cdr(pointer);
            cdr(pointer) = result;
            result = pointer;
            pointer = next_argument;
        }

        // 3. Return the result:
        return result;
    }

    PointerType reverse_copy(PointerType pointer) {

        /*
         * ### Creates a reversed copy of the list given by a pointer.
         *
         * #### Preconditions:
         * 1.a) EITHER pointer = NIL,
         * 1.b) OR     pool contains at least (size_t)pointer cells.

         * #### Postconditions:
         * 1.a) EITHER result is nil,
         * 1.b) OR     result is a valid pointer in terms of this list.
         */

        // 0. Check preconditions:
        assert(pointer == NIL ||
               static_cast<decltype(impl)::size_type>(pointer) <= impl.size());

        // 1. Initialize the result:
        PointerType result = NIL;

        // 2. Iteratively copy elements from input to the output:
        while (pointer != NIL) {
            result = cons(car(pointer), result);
            pointer = cdr(pointer);
        }

        // 3. Return the new list:
        return result;
    }

    struct ConstIterator : std::iterator<std::forward_iterator_tag, Value> {

        /*
         * ## Constant iterator over list pool lists.
         */

        const ListPool *pool;
        PointerType ptr;

        ConstIterator(const ListPool &pool, PointerType ptr) : pool { &pool }, ptr { ptr } {}

        // ConstIterator is semi-regular:
        ~ConstIterator() = default;
        ConstIterator() : pool { nullptr }, ptr { NIL } {}
        ConstIterator(const ConstIterator& x) = default;
        ConstIterator &operator=(const ConstIterator &x) = default;

        // ConstIterator is regular (and equality comparable):
        friend bool operator==(const ConstIterator &x, const ConstIterator &y) {
            // a) Check if both are "end":
            if ((x.pool == nullptr || x.ptr == NIL) &&
                (y.pool == nullptr || y.ptr == NIL)) return true;
            // b) Otherwise, perform a regular comparison:
            return x.pool == y.pool && x.ptr == y.ptr;
        }

        friend bool operator!=(const ConstIterator &x, const ConstIterator &y) {
            return !(x == y);
        }

        // ConstIterator is an Iterator:
        ConstIterator &operator++() {
            ptr = pool->cdr(ptr);
            return *this;
        }

        ConstIterator operator++(int) {
            ConstIterator copy = *this;
            operator++();
            return copy;
        }

        const Value &operator*() const { return pool->car(ptr); }

        // ConstIterator is an input iterator:
        const Value *operator->() {
            return std::addressof(operator*());
        }
    };

    friend ConstIterator begin(const ListPool &pool, PointerType ptr) {
        return { pool, ptr };
    }

    friend ConstIterator end(const ListPool &pool) {
        return { pool, NIL };
    }
};

bool equals(const Value &x, const Value &y, const ListPool &pool);

struct ValueCompare {
    const ListPool &pool;
    bool operator()(const Value &x, const Value&y) const { return equals(x, y, pool); }
};

bool test_list_pool();

#endif
