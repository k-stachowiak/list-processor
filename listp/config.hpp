#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <cstdlib>

using PointerType = std::size_t;
const PointerType NIL = 0;

template <class T>
struct invert {
    T operator()(const T &x) const { return 1 / x; }
};

#endif
