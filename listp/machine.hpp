#ifndef MACHINE_HPP
#define MACHINE_HPP

#include "config.hpp"
#include "data.hpp"
#include "parse.hpp"

#include <string>
#include <map>
#include <functional>
#include <list>
#include <iostream>
#include <algorithm>

#include <cassert>

struct Scope {

    /*
     * ## Represents a single level of symbol visibility.
     *
     * An example of a scope is the global scope. This could be implemented only
     * using std::map, but there may be more types of stack. E.g. for each
     * function execution, a local cope arises, and so happens e.g. for the "let"
     * special forms.
     *
     * Whenever such a local scope appears, the global one is still visible,
     * therefore each scope may define its parent. This may be:
     * - nullptr:          for the global scope,
     * - the global scope: for the function execution scope,
     * - local scope:      for the "let" special form's scope*.
     *
     * * note, that if "let" special forms are nested, each new, deeper scope,
     *   will point to the outer one as its parent. Only the topmost "let" scope
     *   in a given function, will point to the local scope as its parent.
     */

    Scope *parent;
    std::map<std::string, PointerType> map;
    typedef decltype(map)::iterator iterator_type;
};

struct Machine {

    /*
     * ## Represents the execution state.
     */

    /*
     * ### Helper types:
     */
    using BuiltInFunction = std::function<bool(PointerType, Machine&, Value&)>;

    /*
     * ### Debugging state:
     */
    bool debug = false;
    int debug_depth = 0;

    /*
     * ### Basic state:
     */
    ListPool pool;
    std::map<std::string, BuiltInFunction> built_in_functions;

    /*
     * ### Scope management:
     *
     * This is a bit tricky.
     *
     * The scopes may have parents, e.g. a local scope has the global scope as
     * its parent. Having a parent means, that if a lookup fails in a given
     * scope, we may resort to search in its parent. If the parent lookup
     * succeeds we may say that the symbol was available in the original scope.
     *
     * Therefore, a symbol is available in the given scope if it is available in
     * its map, or it is available in its parent.
     *
     * The scopes form a stack. At the bottom, there is the global scope, which
     * always exists. The subsequent scopes appear as we enter a function call
     * or a "let" special form. The following cases are handled in the machine:
     *
     * 1) Code in the global space: only uses the global scope (local_stack is
     *    empty)
     *
     * 2) We enter a function call: we add a new, local scope on the local_stack
     *    its parent is the global scope. Whe have a scope chain:
     *    local -> global -> nullptr
     *    (note, that when we enter a call from a local scope, we do the same,
     *     i.e. we create a new local scope, put in on the stack and set its
     *     parent to the _global_ scope)
     *
     * 3) We enter a inner scope, e.g. in "let" special form: we add a new local
     *    scope, and set the previous local scope as its parent. Whe then have
     *    the following scope chain:
     *    let scope -> local -> global -> nullptr, if let is inside a function,
     *    or:
     *    let scope -> global -> nullptr, if let is in the global scope.
     *    Any further, nested let special form, makes the chain longer by adding
     *    more let scopes at the front.
     *
     * NOTE: the sequence stored in local_stack is not equivalent to the scope
     *       chain (See 2)).
     *
     * The above approach means, that whenever we are leaving a scope, the
     * current scope is:
     * - local_stack.back(): if !local_stack.empty(),
     * - global:             otherwise.
     * In fact, the statement above is an invariant for the whole scope
     * management system.
     */
    Scope global { nullptr, {} };
    std::list<Scope> local_stack;

    Scope *scope_current() {

        /*
         * ### Implements the concept of the current scope.
         */

        // 1. The current scope is denoted by:
        //    - EITHER the back of local_stack (if not empty),
        //    - OR     the global scope (otherwise).
        return local_stack.size() ? (&local_stack.back()) : &global;
    }

    const Scope *scope_current() const {
        return local_stack.size() ? (&local_stack.back()) : &global;
    }

    bool scope_find(const std::string &key, Scope::iterator_type &result) {

        /*
         * ### Tries to find a symbol in the "current scope".
         */

        // 1. Establish the current scope:
        Scope *current = scope_current();

        // 2. Perform the lookup across the scope chain:
        while (current) {
            result = current->map.find(key);
            if (result != current->map.end()) return true;
            current = current->parent;
        }

        // 3. If symbol not found across the scope chain, report failure:
        return false;
    }

    void scope_set(const std::string &key, PointerType ptr) {

        /*
         * ### Assigns a value a symbol in the current scope.
         *
         * #### Notes:
         * - The value will be assigned regardless of it being present in the
         *   current sope or not.
         */

        scope_current()->map[key] = ptr;
    }

    bool scope_get(const std::string &key, PointerType &ptr) {

        /*
         * ### Tries to get a value assigned to a given symbol.
         */

        Scope::iterator_type it;
        if (scope_find(key, it)) {
            ptr = it->second;
            return true;
        } else {
            return false;
        }
    }
};

struct ExecutionScopeDebugger {

    /*
     * ## This is a silly debugging class.
     *
     * Because a lot of evaluation code happens in the return statements, there
     * is no room for the debugging code at the end of the execution functions.
     * Therefore we squeeze the debugging code into the function cleanup code
     * by placing such an object in the local scope and putting the debug code
     * into its constructor and destructor.
     *
     * This class works based on a common pattern used in the debugged code.
     * Most of the functions do some work and report the success or failure via
     * the returned boolean value. If we succeed, we also return the operation's
     * result via a reference to a value object. Objects of this debugging class
     * capture the returned result reference and print out its value upon the
     * destruction.
     *
     * The debugging traps (capturing the result references) may nest. The
     * nesting depth is stored in the machine object, and used to format the
     * debugging messages.
     *
     * Whether the debugging messages are printed or not is also determined
     * at run-time by a flag in the machine's state.
     */

    const std::string function_name;    // What function was called?
    const std::string arguments_string; // With what arguments?
    const Value &result_value;          // What result we are observing?
    Machine &machine;                   // In which machine we operate?
    const std::string indentation;      // How much do we indent our messages?

    ExecutionScopeDebugger(const std::string &function_name,
                           const std::string &arguments_string,
                           const Value &result_value,
                           Machine &machine) :
        function_name(function_name),
        arguments_string(arguments_string),
        result_value(result_value),
        machine(machine),
        indentation(machine.debug_depth * 4, ' ')
    {
        if (machine.debug) {
            std::cout << indentation << "> " << function_name << ": " <<
                arguments_string << std::endl;
            ++machine.debug_depth;
        }
    }

    ~ExecutionScopeDebugger() {
        if (machine.debug) {
            --machine.debug_depth;
            std::cout << indentation << "< " << function_name << " -> " <<
                to_string(result_value, machine.pool) << std::endl;
        }
    }
};

void register_default_built_in_functions(Machine &machine);

bool execute_value(const Value &value, Machine &machine, Value &result);

template <class It>
bool execute_source(It &first, It last, Machine &machine, Value &result) {

    /*
     * ## Evaluate a Lisp expression taken from a character range.
     *
     * ### Arguments:
     * - first:   The beginning of the analyzed range,
     * - last :   the end of the analyzed range,
     * - machine: machine in terms of which to evaluate the expression,
     * - result:  (out) reference under which the result should be written.
     *
     * ### Returns:
     * - true if the execution was successful,
     * - false otherwise.
     *
     * ### Preconditions:
     * 1. source is not empty, and contains at least one non-whitespace
     *    character.
     *
     * ### Postconditions:
     * 1. If true has been returned the result variable holds the execution
     *    result.
     * 2. If true was returned, then:
     *   2.a) EITHER first == last,
     *   2.b) OR     first != last AND *first is not a white space.
     *
     * ### Notes:
     * - even if false was returned, the evaluation may have had an effect on
     *   the underlying machine.
     * - the argument "first" is taken by reference and modified for the
     *   potential further execution.
     */

    ExecutionScopeDebugger scope_debugger(
        "execute_source",
        "\"" + std::string { first, last } + "\"",
        result,
        machine);

    // 0. Check Preconditions:
    first = std::find_if_not(first, last, isspace);
    assert(first != last);

    // 1. Parse input:
    //    (call to parse_expression secures the Postconditions)
    Value expression;
    if (!parse_expression(first, last, machine.pool, expression)) {
        std::cerr << "Failed parsing an expression for execution" << std::endl;
        return false;
    }

    // 2. Execute the parsed value:
    return execute_value(expression, machine, result);
}

bool test_execution();

#endif
