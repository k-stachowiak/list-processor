#include "tokenize.hpp"

#include <iostream>
#include <vector>

namespace {

bool test_tokenizer_case(const std::string &input, const std::vector<std::string> &expected) {
    auto first = input.begin(), last = input.end();
    auto expected_first = expected.begin();
    while (first != last && expected_first != expected.end())
        if (next_token(first, last) != *expected_first++)
            goto failure;
    if (first != last || expected_first != expected.end())
        goto failure;
    return true;
failure:
    std::cerr << "Failed tokenizer test for \"" << input << "\"" << std::endl;
    return false;
}

}

bool test_tokenizer() {
    std::vector<std::string> expected_1 = {
        std::string { "(" },
        std::string { "*" },
        std::string { "2" },
        std::string { "(" },
        std::string { "+" },
        std::string { "2" },
        std::string { "2" },
        std::string { ")" },
        std::string { ")" }
    };
    std::vector<std::string> expected_2 = {
        std::string { "(" },
        std::string { "(" },
        std::string { "(" },
        std::string { "(" },
        std::string { "(" },
        std::string { "(" }
    };
    return
        test_tokenizer_case("", {}) &&
        test_tokenizer_case("(* 2 (+ 2 2))", expected_1) &&
        test_tokenizer_case(" ( * 2 ( + 2 2 ) ) ", expected_1) &&
        test_tokenizer_case("((((((", expected_2) &&
        test_tokenizer_case(" (( (((( ", expected_2);
    // The code below should trigger assertion:
    // test_tokenizer_case("  ", {});
    // ...and so should this:
    // std::string s; next_token(s.begin(), s.begin());
}
