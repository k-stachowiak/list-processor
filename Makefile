export CXXFLAGS = -std=c++11 -g -O0 -Wall -Wextra

list-processor: list-processor.cpp listp/listp.hpp listp/config.hpp listp/machine.hpp listp/data.hpp listp/parse.hpp listp/tokenize.hpp listp/listp.a
	$(CXX) $(CXXFLAGS) list-processor.cpp listp/listp.a -o $@

.PHONY: listp/listp.a clean

listp/listp.a:
	$(MAKE) -C listp

clean:
	rm -rf list-processor
	$(MAKE) -C listp clean
