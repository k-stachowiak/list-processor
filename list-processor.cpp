#include "listp/listp.hpp"

#include <stdexcept>
#include <iostream>

// Next tasks
// ==========
// - Add tests for analythical algorithms.
// - Consider registering locations for all the values that are parsed.
// - Add strings
// - Implement string pool for sharing symbol values (copy on write?)
// - Implement garbage collection
// - Implement a real machine.

// Glossary
// ========
//
// ## Precondition:
// A condition, in the context of a function execution, that is expected to be
// guaranteed by the caller. If checked in the function, it must terminate the
// program upon failure. Doesn't need to be checked by the callee.
//
// ## Expectation:
// A condition, in the context of a function execution, that is expected to be
// satisfied by the input, but is to be checked by the callee. Upon failure, the
// function reports an error, but doesn't terminate the program. Expectation
// must be checked by the callee.
//
// ## Postcondition:
// A state that must be guaranteed by a callee before returning. There is an
// implicit postcondition, that if a function succeeds, Expectations must have
// been satisfied.

namespace {

bool test() {
    return
        test_value() &&
        test_tokenizer() &&
        test_list_pool() &&
        test_parser() &&
        test_execution();
}

bool repl() {
    Machine machine;
    register_default_built_in_functions(machine);
    std::string line;
    std::cin >> std::noskipws;
    std::cout << "> ";
    while (std::getline(std::cin, line)) {
        TrackingIterator<typename std::string::iterator>
            first { line.begin() }, last { line.end() };
        while (first != last) {
            Value result;
            if (!execute_source(first, last, machine, result))
                std::cerr << "REPL error" << std::endl;
            std::cout << "< " << to_string(result, machine.pool) << std::endl;
            std::cout << "> ";
        }
    }
    return true;
}

}

// Entry point
// ===========

int main(int argc, char **argv) try {
    if (argc == 2 && strcmp(argv[1], "test") == 0)
        return test() ?
            EXIT_SUCCESS :
            EXIT_FAILURE;
    else
        return repl() ?
            EXIT_SUCCESS :
            EXIT_FAILURE;

} catch (const std::exception &ex) {
    std::cerr << "Exception caught: " << ex.what() << std::endl;
    return 1;
}
